﻿using System;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using ShowDate.Workers;

namespace ShowDate
{
	class Tray
	{
		private NotifyIcon trayIcon;
		private ContextMenuStrip contextTrayMenu = new ContextMenuStrip();

		public void SysTrayApp()
		{
			// Create tray menu items
			contextTrayMenu.Items.Add(new ToolStripMenuItem("Show times in cities", Properties.Resources.time, ShowTimesInCities));
			contextTrayMenu.Items.Add(new ToolStripMenuItem("Exit", Properties.Resources.Exit, MenuItemOnExit));

			// Create a tray icon.
			trayIcon = new NotifyIcon
			{
				Icon = System.Drawing.Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location),
				ContextMenuStrip = contextTrayMenu,
				Visible = true,
			};
			trayIcon.MouseMove += MouseMove;
			trayIcon.MouseClick += MouseClick;

		}

		private string GetCurrentDate()
		{
			using (var w = new DatesWorker())
			{
				return w.GenerateString();
			}
		}

		private void ShowTimesInCities(object sender, EventArgs e)
		{
			using (TimeZonesWorker w = new TimeZonesWorker())
			{
				var cities = w.GetCitiesInfo("TimeZones.xml");
				foreach (var item in cities)
				{
					var s = String.Format("{0}: {1}", item.CityName, item.CityLocalTimeStr);
					//trayIcon.BalloonTipTitle = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
					trayIcon.BalloonTipText = s;
					trayIcon.ShowBalloonTip(1);
				}
			}
		}

		private void MouseMove(object sender, EventArgs e)
		{
			trayIcon.Text = GetCurrentDate();
		}

		private void MouseClick(object sender, EventArgs e)
		{
			if ((e as MouseEventArgs).Button == MouseButtons.Left && !trayIcon.ContextMenuStrip.Visible)
			{
				ShowTimesInCities(sender, e);
			}
		}


		private void MenuItemOnExit(object sender, EventArgs e)
		{
			trayIcon.Dispose();
			Application.Exit();
		}
	}
}
