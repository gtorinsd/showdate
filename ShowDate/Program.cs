﻿using System.Windows.Forms;

namespace ShowDate
{
	class Program
	{
		static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault(false);

			Tray tray = new Tray();
			tray.SysTrayApp();

			Application.Run();
		}
	}
}
