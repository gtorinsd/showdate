﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace ShowDate.Workers
{
	struct Cities
	{
		public string CityName;
		public string CityLocalTimeStr;
	}

	class TimeZonesWorker: IDisposable
	{
		private string GetTimeZoneIdForUtc(string utcOffset)
		{
			var item = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(i => i.BaseUtcOffset.Hours == Int32.Parse(utcOffset));
			if (item != null)
				return item.Id;
			else
				return string.Empty;
		}

		public List<Cities> GetCitiesInfo(string xmlFileName)
		{
			var cities = new List<Cities>();
			using (StreamReader sr = new StreamReader(xmlFileName, Encoding.Default))
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(sr);
				foreach (XmlNode node in doc.DocumentElement.ChildNodes)
				{
					string name = node.ChildNodes[0].InnerText;
					string utcOffset = node.ChildNodes[1].InnerText;

					Cities city = new Cities
					{
						CityName = name
					};
					
					var id = GetTimeZoneIdForUtc(utcOffset);
					if (string.IsNullOrEmpty(id))
					{
						//throw new TimeZoneNotFoundException("Wrong UTC time zone for " + name);
						city.CityLocalTimeStr = "ERROR";
					}
					else
					{
						var cityUtcTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(id));
						city.CityLocalTimeStr = cityUtcTime.ToShortTimeString();
					}

					cities.Add(city);
				}
				return cities;
			}
		}

		public void Dispose()
		{
			//throw new NotImplementedException();
		}
	}
}
