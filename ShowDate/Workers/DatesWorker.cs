﻿using System;
using System.Text;

namespace ShowDate.Workers
{
	class DatesWorker: IDisposable
	{
		private string[] _rusDays = { "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье" };
		private string[] _ukrDays = { "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя" };
		// Last item of months array is a symbol of year.
		private string[] _rusMonths = { "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря", "г." };
		private string[] _ukrMonths = { "січня", "лютого", "березня", "квітня", "травня", "червля", "липня", "серпня", "вересня", "жовтня", "листопада", "грудня", "р." };

		private string GetDate(string[] daysOfWeek, string[] months)
		{
			DateTime date = DateTime.Today;
			return string.Format("{0}, {1} {2} {3} {4}", daysOfWeek[(int) date.DayOfWeek - 1], date.Day, months[date.Month - 1], date.Year, months[months.Length - 1]);
		}

		public string GenerateString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(GetDate(_rusDays, _rusMonths));
			sb.AppendLine(GetDate(_ukrDays, _ukrMonths));
			return sb.ToString().Trim();
		}

		public void Dispose()
		{
			//throw new NotImplementedException();
		}
	}
}
